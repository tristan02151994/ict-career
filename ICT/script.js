var data = [
    {Role: "Software_Developer_&_Programmer", Job_Vacancies_Aug_2018: 590, Salary_Max: 100, Salary_Min: 72,
        describe: "Software developers and programmers develop and maintain computer software, websites and software applications (apps).",
        require: ["* computer software and systems", "* programming languages and techniques", "* software development processes such as Agile", "* confidentiality, data security and data protection issues"],
        img: "sdp"},
    {Role: "Database_&_Systems_Administration", Job_Vacancies_Aug_2018: 74, Salary_Max: 90, Salary_Min: 66,
        describe: "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures.",
        require: ["* a range of database technologies and operating systems", "* new developments in databases and security systems", "* computer and database principles and protocols"],
        img: "dsa"},
    {Role: "Help_Desk_&_IT_Support", Job_Vacancies_Aug_2018: 143, Salary_Max: 65, Salary_Min: 46,
        describe: "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software.",
        require: ["* computer hardware, software, networks and websites", "* the latest developments in information technology"],
        img: "hdits"},
    {Role: "Data_Analyst", Job_Vacancies_Aug_2018: 270, Salary_Max: 128, Salary_Min: 69,
        describe: "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        require: ["* data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R", "* data analysis, mapping and modelling techniques", "* analytical techniques such as data mining"],
        img: "da"},
    {Role: "Test_Analyst", Job_Vacancies_Aug_2018: 127, Salary_Max: 98, Salary_Min: 70,
        describe: "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        require: ["* programming methods and technology", "* computer software and systems", "* project management"],
        img: "st"},
    {Role: "Project_Management", Job_Vacancies_Aug_2018: 188, Salary_Max: 190, Salary_Min: 110,
        describe: "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        require: ["* principles of project management", "* approaches and techniques such as Kanban and continuous testing", "* how to handle software development issues", "* common web technologies used by the scrum team"],
        img: "pm"},
    {Role: "", Job_Vacancies_Aug_2018: "", Salary_Max: "", Salary_Min: ""}
    ];

function f() {
    makeTable();
    detail();
}
function makeTable() {  // to create a table and add data
    var tHead = $(".thead-dark").append("<tr>" + "<th>" + "Role" + "</th>" + "<th>" + "Job Vacancies 08/2018" + "</th>" + "<th>" + "Salary Max" + "</th>" + "<th>" + "Salary Min" + "</th>" + "</tr>");
    for (var i = 0; i < data.length; i++) {
        if (i <= 5) {
        var tBody = $(".positions").append("<tr>" + "<td>" + data[i].Role + "</td>" + "<td>" + data[i].Job_Vacancies_Aug_2018 + "</td>" + "<td>" + data[i].Salary_Max + "</td>" + "<td>" + data[i].Salary_Min + "</td>" + "</tr>");
        }
        else {
            tBody = $(".positions").append("<tr>" + "<td>" + data[i].Role + "</td>" + "<td>" + total() + "</td>" + "<td>" + avgMax() + "</td>" + "<td>" + avgMin() + "</td>" + "</tr>");
        }
    }

    function total() {  //to calculate the total number for Job_Vacancies_Aug_2018
        var t = 0;
        for (var i = 0; i < data.length - 1; i++) {
            t += data[i].Job_Vacancies_Aug_2018;
        }
        return "Total: " + t;
    }

    function avgMax() {  // to calculate the average number for Salary_Max
        var t = 0;
        for (var i = 0; i < data.length - 1; i++) {
            t += data[i].Salary_Max;
        }
        return "Avg: " + Math.round(t / (data.length - 1));

    }

    function avgMin() { // to calculate the average number for Salary_Min
        var t = 0;
        for (var i = 0; i < data.length - 1; i++) {
            t += data[i].Salary_Min;
        }
        return "Avg: " + Math.round(t / (data.length - 1));
    }
}

function detail() {  // When you click each row, the information above the table will change.
    $(document).on("click", "tr", function () {
        var role = this.firstChild.textContent;
        for (var i = 0; i < data.length-1; i++) {
            if (role == data[i].Role) {
                $(".detail")[0].src = "../ICT/" + data[i].img + ".jpg";
                $("span")[1].textContent = data[i].Role;
                $(".detail")[2].textContent = data[i].describe;
                $("dd").hide();
                for (var i0 = 0; i0 < data[i].require.length; i0++) {
                    var require = $("dl").append("<dd>" + data[i].require[i0] + "</dd>");
                }
            }
        }
    });
}
